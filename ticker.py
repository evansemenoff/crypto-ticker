from requests import get as GET

UP, DOWN, WHITE = "\033[1;32;40m + ", "\033[1;31;40m - ", "\033[0;37;40m"
URL = "https://api1.binance.com/api/v3/ticker/price?symbol="


class Ticker:

    def __init__(self, symbol, fiat):
        self.symbol = symbol
        self.sign = "   "
        self.queryURL = URL + symbol + fiat
        d = GET(self.queryURL).json()
        self.price = float(d['price'])
        self.percent_change = "(0.00)"
        self.value_change = "$0.00"

    def __str__(self):
        return WHITE \
               + self.symbol.ljust(6) \
               + "$" + f'{self.price:.2f}'.ljust(10) \
               + self.sign \
               + self.value_change.ljust(10) \
               + self.percent_change

    def update_price(self):
        query_data = GET(self.queryURL).json()
        new_price = float(query_data['price'])
        self.sign = UP if new_price > self.price else DOWN
        if new_price == self.price:
            self.sign = "   "
        self.percent_change = "(%" + f'{abs(((new_price / self.price) * 100) - 100):.2f}' + ")"
        self.value_change = "$" + f'{abs(new_price - self.price):.2f}'
        self.price = new_price
