from ticker import Ticker


class TickerList:

    def __init__(self, ticker_list, fiat):
        self.ticker_list = [Ticker(t, fiat) for t in ticker_list]

    def update(self):
        for t in self.ticker_list:
            t.update_price()

    def print_tickers(self):
        for i in range(len(self.ticker_list)):
            if i != len(self.ticker_list):
                print(str(self.ticker_list[i]))
            else:
                print(str(self.ticker_list[i]), end="")

