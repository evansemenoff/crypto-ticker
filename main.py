from tickerList import *
from time import sleep
from subprocess import call
from cursor import hide, show
from sys import argv
from atexit import register


def shutdown():
    show()
    call('clear')


if __name__ == "__main__":
    register(shutdown)
    hide()
    fiat = argv[1]
    symbols = argv[2:]
    ticker_list = TickerList(symbols, fiat)
    while ...:
        ticker_list.update()
        call('clear')
        ticker_list.print_tickers()
        sleep(1)
